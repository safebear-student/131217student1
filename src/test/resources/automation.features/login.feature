Feature: Login

User story:
In order to access the application
as a user
I need to be able to log in

  Rules:
  - User must be able to reset password
  - It must be secure
  - It must be able to handle two factor authentication

  ToDo:
  - Two factor authentication

  Questions:
  - Minimum password character length
  - Password rules - characters/special characters/ numbers
  - Can people have the same password?

  Domain Language:
  - Permissions to set up are administrator access to everything
  - User = access only to user page
  - account = bank account
  - Administrator = access to everything

  Background:
    Given I have a user testuser with user permissions and password testing
    And I have a user simon with admin permissions and password S@febear

  @high-risk
  Scenario: A normal user logs in successfully
    When I login with testuser and password testing
    Then I can see my account

    @to-do
    @high-risk
    Scenario Outline: a user has forgotten their password
      Given I have a user bob with risk permissions and password test
      When a <user> has forgotten their password
      Then an administrator can reset it
      Examples:
      |user|
      |tom |
      |debs|

