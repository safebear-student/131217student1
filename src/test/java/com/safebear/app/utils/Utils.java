package com.safebear.app.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Utils {
    private WebDriver driver;
    private String url;
    private String browser;
    Properties prop = new Properties();
    InputStream input = null;
    private int sleepTime;

    public Utils() {
        try {
            String file = "config.properties";
            //reading in the file
            input = Utils.class.getClassLoader().getResourceAsStream(file);
            // code to cope if the file isn't present
            if (input == null) {
                System.out.println("Unable to find file " + file);
                return;
            }
            //loading the properties
            prop.load(input);

            //storing the URL property in our 'url' variable
            this.url = prop.getProperty("url");
            //storing the browser property in our 'browser' variable
            this.browser = prop.getProperty("browser");
            //get sleep time
            this.sleepTime = Integer.parseInt(prop.getProperty("sleepTime"));

        } catch (IOException ex) {
            ex.printStackTrace();
            //after reading in the file we also have to close it and tidy up after ourselves
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //this chooses which WebDriver Selenium will use ChromeDriver or InternetExplorerDriver etc
        switch (browser) {
            case "chrome":
                this.driver = new ChromeDriver();
                break;
            case "ie":
                this.driver = new InternetExplorerDriver();
                break;
            case "chrome_headless":
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("headless");
                this.driver = new ChromeDriver(chromeOptions);
                break;
            default:
                this.driver = new ChromeDriver();
                break;
        }

    }

    public WebDriver getDriver() {
        return driver;
    }

    public String getUrl() {
        return url;
    }

    public int getSleepTime() {
        return sleepTime;
    }
}
