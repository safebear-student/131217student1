package com.safebear.app;

import com.safebear.app.pages.WelcomePage;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {

    @Test
    public void testLogin(){
        //Step 1 Check we are on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());

        //Step 2 Click on the Login link and the Login Page loads
        //Alternative way of doing the same test
        //assertTrue(welcomePage.clickOnLogin(this.loginPage));
        welcomePage.clickLogin();
        assertTrue(loginPage.checkCorrectPage());

        // Step 3 Login
        loginPage.enterUsername("testuser");
        loginPage.enterPassword("testing");
        loginPage.login();
        assertTrue(userPage.checkCorrectPage());

        //Step 4 Logout
       loginPage.logOut();
//       assertFalse(loginPage.isLogOutLinkVisible());
    }

    @Test
    // Test 2 in the additional exercises book
    public void testSendText()
    {
        //Step 1 Check we are on the Welcome Page - cick on the Login link and the Login Page loads
        assertTrue(welcomePage.checkCorrectPage());

        welcomePage.clickLogin();
        assertTrue(loginPage.checkCorrectPage());

        // Step 2 Send some text to the input fields and check this is populated
        loginPage.sendTextToAllInputs("test");
        assertTrue(loginPage.getUsernameValue().compareToIgnoreCase("test")==0);
        assertTrue(loginPage.getPasswordValue().compareToIgnoreCase("test")==0);

        // Step 3 Click on the Home menu link and verify the welcome page is displayed
        loginPage.goToHome();
        assertTrue(welcomePage.checkCorrectPage());
    }
}
