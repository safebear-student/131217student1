package com.safebear.app;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue={"com.safebear.app"},
        features={"classpath:automation.features/login.feature"},
        tags={"~@to-do"}
)

public class MyCukeRunner{}