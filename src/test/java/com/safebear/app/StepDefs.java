package com.safebear.app;

import com.safebear.app.pages.LoginPage;
import com.safebear.app.pages.UserPage;
import com.safebear.app.pages.WelcomePage;
import com.safebear.app.utils.Utils;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class StepDefs {
    WebDriver driver;
    Utils utility;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;


    @Before
    // Has to be setup again in Cucumber
    public void setUp()
    {
        utility = new Utils();
        this.driver = utility.getDriver();

        welcomePage = new WelcomePage(driver);
        userPage = new UserPage(driver);
        loginPage = new LoginPage(driver);

        this.driver.get(utility.getUrl());
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown()
    {
        driver.quit();
    }

    @Given("^I have a user (.+) with (.+) permissions and password (.+)$")
    public void create_user(String username, String permissions, String password) throws Throwable {
        System.out.println("username: " + username + ", permissions: " + permissions + ", password: " + password);
    }

    @When("^I login with (.+) and password (.+)$")
    public void user_logs_in(String username, String password) throws Throwable {
        welcomePage.clickLogin();
        loginPage.enterUsername(username);
        loginPage.enterPassword(password);
        loginPage.login();
    }

    @Then("^I can see my account$")
    public void user_views_account() throws Throwable {
        assertTrue(userPage.checkCorrectPage());
    }


}
