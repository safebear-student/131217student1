package com.safebear.app.pages;

import com.safebear.app.BaseTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WelcomePage extends BasePage{
    WebDriver driver;

    @FindBy(linkText = "Login")
    WebElement loginLink;

    public WelcomePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrectPage(){
        return driver.getTitle().startsWith("Welcome");
    }

    /*public boolean clickOnLogin(LoginPage loginPage){
       loginLink.click();
       return loginPage.checkCorrectPage();
    }*/

    public void clickLogin() {
        loginLink.click();
    }
    }
