package com.safebear.app.pages;

import com.safebear.app.BaseTest;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.support.PageFactory;

public class UserPage extends BasePage{
          WebDriver driver;

        public UserPage(WebDriver driver){
            this.driver = driver;
            PageFactory.initElements(driver,this);
        }

        public boolean checkCorrectPage(){
            return driver.getTitle().startsWith("Logged");
        }
}
