package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class LoginPage extends BasePage{
    WebDriver driver;
    WebDriverWait wait;

    /*@FindBy(id="myid")
    WebElement username_field;*/

    // use xPath instead to find the username field note \ are so it doesn't treat " as end of code
    @FindBy(xpath="//input[id=\"myid\" or @name=\"myid\"]")
    WebElement username_field;

    /*@FindBy(id="mypass")
    WebElement password_field;*/

    // use xPath instead to find the password field note \ are so it doesn't treat " as end of code
    @FindBy(xpath="//input[id=\"mypass\" or @name=\"mypass\"]")
    WebElement password_field;

    //Find the submit button
    @FindBy(xpath="//button[@type=\"submit\"]")
    WebElement submit_button;

    //Find all using the contains syntax for xPath
    @FindAll({
            @FindBy(xpath ="//*[contains(@id,'my')]"),
    })
    List<WebElement> input_fields;

    public LoginPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, 10);
        PageFactory.initElements(driver,this);
    }

    /*public boolean checkCorrectPage(){
        return driver.getTitle().startsWith("Sign");
    }*/

    // Refactored above method to use explicit waits, which is a better practise
    public boolean checkCorrectPage() {
        try {
            this.wait.until(ExpectedConditions.titleIs("Sign In"));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void enterUsername(String username)
    {
       // username_field.sendKeys(username);
        //alternative way of calling the same field
        input_fields.get(0).sendKeys(username);
    }

    public void enterPassword(String password)
    {
        //password_field.sendKeys(password);
        //alternative way of calling the same field
        input_fields.get(1).sendKeys(password);

    }

    public void login()
    {
        //password_field.submit();
        submit_button.click();
    }

    public void sendTextToAllInputs(String text)
    {
        for (int i=0; i < input_fields.size(); i++)
        {
            input_fields.get(i).sendKeys(text);
            //check the field is populated
            wait.until(ExpectedConditions.textToBePresentInElementValue(input_fields.get(i), text));
           // input_fields.get(i).getAttribute()
        }
        // Java 8 alternative shortcut to the above loop but less readable
        /*for (WebElement input_field: input_fields)
        {
            input_field.sendKeys(text);
        }*/
    }

    public String getUsernameValue()
    {
        return username_field.getAttribute("value");
    }

    public String getPasswordValue()
    {
        return password_field.getAttribute("value");
    }


}
