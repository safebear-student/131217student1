package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BasePage {
    WebDriver driver;

    @FindBy(linkText = "Logout")
    WebElement logout;

    @FindBy(linkText = "Home")
    WebElement home;

    public boolean isLogOutLinkVisible()
    {
        return logout.isDisplayed();
    }

    public void logOut()
    {
       if(isLogOutLinkVisible()){
           logout.click();
       }

    }

    public boolean isHomeLinkVisible()
    {
        return home.isDisplayed();
    }

    public void goToHome()
    {
        if(isHomeLinkVisible()){
            home.click();
        }

    }

}
